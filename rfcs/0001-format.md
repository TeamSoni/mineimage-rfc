MineImage Format
================

A MineImage is a PNG file.

All MineImage PNGs must have the chunks `IHDR`, `IDAT` and `IEND`.
