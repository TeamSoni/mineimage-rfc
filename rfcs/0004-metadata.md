MineImage Metadata
==================

Metadata starts on the scanline after block data.

MineImage Metadata uses an adaptation of the [NBT format](https://minecraft.gamepedia.com/NBT_format).

To convert a pixel with 8-bit channels r, g, b, a into a 32-bit integer, the following conversion shall be used:

    r | (g << 8) | (b << 16) | ((255-a) << 24)

MineImage Metadata is a set of pixels, parsed from left to right, top to bottom (scanline order).

In standard NBT, each tag is represented with a single byte tag type, followed by a 2-byte name length. In MineImage, they're both stored in a single pixel.

The conversion from NBT 8-bit tag type (t) + 16-bit name length (l) to a 32-bit integer shall be:

    t | (l << 8)

With the following exceptions:

- If `t` is `0`, `l` must be 0.
- If `t` is `1`, an additional 8-bit field `v` shall be used and the conversion shall be:

        t | (l << 8) | (v << 24)

- If `t` is `9`, an additional 8-bit field `i` shall be used and the conversion shall be:

        t | (l << 8) | (i << 24)

Strings
-------

Strings in NBT are UTF-8. Strings in MineImage are also UTF-8, but they're stored in 32-bit chunks.

Each 32-bit chunk in a string shall be stored as a separate pixel. If the string length isn't a multiple of 4, it shall be padded, at the end, with ASCII NUL.

Types and their respective representations (in network byte order)
------------------------------------------------------------------

`n` shall be the tag name, `v` shall be the tag value. Note that all tags are 32-bit aligned.

    ID - Type           - Bytes                                   - Notes
                        - Tag       -Name-Payload                 -
    0  - TAG_End        - 00 00 00 00                             -
    1  - TAG_Byte       - vv ll ll 01 [n]                         -
    2  - TAG_Short      - 00 ll ll 02 [n] 00 00 vv vv             - 
    3  - TAG_Int        - 00 ll ll 03 [n] vv vv vv vv             -
    4  - TAG_Long       - 00 ll ll 04 [n] v1 v1 v1 v1 v2 v2 v2 v2 - (v1 << 32) | v2
    5  - TAG_Float      - 00 ll ll 05 [n] vv vv vv vv             - Float.intBitsToFloat(v)
    6  - TAG_Double     - 00 ll ll 06 [n] v1 v1 v1 v1 v2 v2 v2 v2 - Double.longBitsToDouble((v1 << 32) | v2)
    7  - Tag_Byte_Array - 00 ll ll 07 [n] ss ss ss ss [b]         - s is number of bytes, b is a byte, [b] is stored like a string (split into 32-bit chunks, pad with 00).
    8  - TAG_String     - 00 ll ll 08 [n] 00 00 ss ss [v]         - s is number of bytes, [v] is a string.
    9  - TAG_List       - ii ll ll 09 [n] ss ss ss ss [?]         - s is number of payloads.
        ? is just the payload part of the tag of type i. TAG_Byte's v and TAG_List's i shall be stored separately in the payload.
        That is, a List containing 3 Bytes, 1, 2 and 3, in that order, looks like:
            01 ll ll 09 [n] 00 00 00 03 00 00 00 01 00 00 00 02 00 00 00 03
        And a List containing a single empty List looks like:
            09 ll ll 09 [n] 00 00 00 01 00 00 00 00 00 00 00 00
    10 - TAG_Compound   - 00 ll ll 0A [n] [?]                     - ? is a tag. Stops at the first TAG_End.
    11 - TAG_Int_Array  - 00 ll ll 0B [n] ss ss ss ss [i]         - s is number of ints, i is an int.
