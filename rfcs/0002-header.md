MineImage Header
================

A MineImage file shall start with 3 pixels indicating the width, height and depth (X length, Y length and Z length, respectively).

The length pixels shall be converted to 32-bit integers as follows:

    r | (g << 8) | (b << 16) | ((255-a) << 24)

Where r, g, b, a are 8-bit channel values. This is so lower pixel values aren't transparent.
