MineImage Blocks
================

Block data starts on the scanline after the header.

Block pixels shall be converted into 32-bit integers as follows:

    r | (g << 8) | (b << 16) | ((255-a) << 24)

Where r, g, b, a are 8-bit channel values.

The blocks section is a set of 2D images.

The first 2D image shall have a width equivalent to the X length of the MineImage, and a height equivalent to the Z length. The number of such images shall be equivalent to the Y length of the MineImage.

Given a MineImage with X length = 2, Z length = 3 and Y length = 4, the following pixel arrangement:

    [1a][1b][2a]
    [1c][1d][2c]
    [1e][1f][2e]
    [2b][3a][3b]
    [2d][3c][3d]
    [2f][3e][3f]
    [4a][4b][  ]
    [4c][4d][  ]
    [4e][4f][  ]

Shall produce the following 4 layers:

    [1a][1b]
    [1c][1d]
    [1e][1f]

    [2a][2b]
    [2c][2d]
    [2e][2f]

    [3a][3b]
    [3c][3d]
    [3e][3f]

    [4a][4b]
    [4c][4d]
    [4e][4f]

Where the top-left of the first layer is position 0,0,0, and the bottom right of the last layer is position 1,3,2.
