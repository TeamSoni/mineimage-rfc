MineImage
=========

MineImage is an image-based serialization format for parts of a Minecraft world.

MineImage files are basically image-based .schematic files.
